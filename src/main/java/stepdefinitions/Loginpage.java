package stepdefinitions;



import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import junit.framework.Assert;

import java.util.concurrent.TimeUnit;

import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Loginpage {
WebDriver driver;
	
	@Given("^User is already on Login page$")
	public void user_is_already_on_login_page() {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		   driver.get("https://www.n11.com/");
	}
	@When("^Title of Loginpage is N11$")
public void title_of_loginpage_is_N11() {
		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("n11.com - Alışverişin Uğurlu Adresi", title);
	
	
	}

	@Then("^User clicks Giris yap$ ")
	public void user_clicks_Giris_yap() {
		driver.findElement(By.className("btnSignIn")).click();
	}
	
	@Then("^User entes email and password and clicks$")
	public void user_enters_email_and_password_and_clicks(String string) {
		 driver.findElement(By.id("email")).sendKeys("gkhnozhn@hotmail.com");
		 WebElement passW = driver.findElement(By.id("password"));
	  passW.sendKeys("Gokce22@");
			//driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);      
		  WebElement logIn =	driver.findElement(By.className("green_flat"));
		  JavascriptExecutor js = (JavascriptExecutor)driver;
		  js.executeScript("arguments[0].click();", logIn);
		  
}
	
	@Then("^User is on Homepage$")
	public void User_is_on_Homepage() {
		 WebElement srch = driver.findElement(By.id("searchData"));
		 srch.clear();
	}
	
	@Given("^write text element Samsung$" )
	public void write_text_Samsung() {
		WebElement srch = driver.findElement(By.id("searchData"));
		srch.sendKeys("Samsung");
		 srch.click();
	}
		@When ("^assert pageContains Samsung$")
		public void assert_pageContains_Samsung() {
			
				 WebElement searchResults = driver.findElement(By.className("resultText "));
		 Asserts.notNull(searchResults, null);
	}
	@Then ("^go to secondPage $")
	public void go_to_secondPage() {
		driver.findElement(By.xpath("//*[@id=\"contentListing\"]/div/div/div[2]/div[4]/a[2]")).click();
	}
	@Given("^ pick third item and add it in favorites$")
		public void pick_third_item_and_add_it_in_favorites(){
		driver.findElement(By.xpath("//*[@id=\"p-415133116\"]/div[1]/span")).click();
		String title = driver.getTitle();
		Assert.assertEquals("Favorilere ekle", title);
		}
	@When("^click favorites $")
	public void click_favorites() {
		driver.findElement(By.xpath("//*[@id=\"header\"]/div/div/div[2]/div[2]/div[2]/div[2]/div/a[2]")).click();

	}
@Then("^remove item$")
public void remove_item() {
	driver.findElement(By.xpath("//*[@id=\"myAccount\"]/div[3]/ul/li[1]/div/a/h4")).click();
	driver.findElement(By.xpath("//*[@id=\"p-415133116\"]/div[3]/span")).click();
	 WebElement dlt = driver.findElement(By.xpath("//*[@id=\"p-415133116\"]/div[3]/span"));
	 Asserts.notNull(dlt, null);
}
@Then("^close browser ")
public void close_browser() {
	driver.quit();
}
}
	

